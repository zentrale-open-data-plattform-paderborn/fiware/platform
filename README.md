# APInf Platform

## General
For API consumers API management provides simple token management, usage analytics and API discovery along with API documentation. API-managers have simplified workflow for common tasks, such as key management, rate limiting and viewing API usage analytics. API management offers additional control on API access, API documentation and analytics in one package.

API Management is a central part and used to set Tenant accesses, host API documenation and provide analytics. Usage in relevant parts are described in this documentation.

Technically it is possbile to expose Context Broker or QuantumLeap directly to Internet, but this is not recommeded. Context Broker or QuantumLeap do not offer any restrictions; this scenario would allow anyone to do any operation (Think DELETE / PUT /POST) on those components.

Original documentation can be found here: https://github.com/apinf/platform

## Usage of APInf Platform
APInf Platform is used to host API documenation and Tenant access (https://apinf-fiware.readthedocs.io/en/latest/#tenant-manager-ui). Login to the platform can be done utilising Keyrock accounts via FIWARE login. Tenants can be created and maintained in the Platform. Bearer tokens and x-api-keys can be fetched from the platform (https://apinf-fiware.readthedocs.io/en/latest/#tenant-manager-ui - RETRIEVING A TOKEN PROGRAMMATICALLY). Admins can view analytics, and basic monitoring on the API health can be performed.

Or you can watch this Youtube-Video: https://www.youtube.com/watch?v=r4eE16nb1Yg

## Versioning  
Tested on:  
Version apinf/platform:0.60.0 (DockerHub digest: sha256:ae1047c8d9258f920f8c0108b9a455aec03d2f9cdc86a222f3ca772265a94655)

## Volume Usage
This component does not use persistent volumes.

## Load-Balancing
Currently only one Replica is deployed.

## Route (DNS)
Apinf Platform UI can be accessed via: https://apis.fiware-dev.opendata-CITY.de

## Access the UI
APInf Platform Administrative user is created during initial configuration of the component, [here](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/tree/master/platform-setup). First registered user in the Platform will become the system administrator.  
After configuration is complete, users can log in using Keyrock credentials with option 'Login with FIWARE'.

## Further Testing
The image is taken as is. No further component testing is done.

## Deployment
In order to deploy, following Gitlab-Variables and K8s-secrets are needed:
### Gitlab-Variables
APINF_DEV_DOMAIN - This variable contains the domain name used for the component in dev-stage.  
APINF_PROD_DOMAIN - This variable contains the domain name used for the component in production-stage.  
APINF_STAGING_DOMAIN - This variable contains the domain name used for the component in staging-stage.  
KUBECONFIG - This variable contains the content of the Kube.cfg used to access the cluster for dev- and staging-stage.  
KUBECONFIG_PROD - This variable contains the content of the Kube.cfg used to access the cluster for prod-stage.  
NAMESPACE_DEV - This variable contains the namespace for dev-stage used by k8s  
NAMESPACE_STAGING - This variable contains the namespace for staging-stage used by k8s  
NAMESPACE_PROD - This variable contains the namespace for production-stage used by k8s  
### Kubernetes Secrets
This project does not use Kubernetes secrets.

## Funding Information
The results of this project were developed on behalf of the city of Paderborn within the funding of the digital model region Ostwestfalen-Lippe of the state of North Rhine-Westphalia.

![img](img/logoleiste.JPG)

## License
Copyright © 2020 Profirator Oy, HYPERTEGRITY AG, omp computer gmbh

This work is licensed under the EUPL 1.2. See [LICENSE](LICENSE) for additional information.
